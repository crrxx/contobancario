import java.time.LocalDate;

public class Movimento{
    private LocalDate dataRichiesta;
    private LocalDate dataValuta;
    private String causale;
    private String tipologia;
    private double importo;


    public Movimento(LocalDate dataRichiesta, LocalDate dataValuta, String causale, String tipologia, double importo){
      this.dataRichiesta = dataRichiesta;
      this.dataValuta = dataValuta;
      this.causale = causale;
      this.tipologia = tipologia;
      this.importo = importo;
    }

    public void visualizzaDatiMovimento(){
      System.out.println("Data Richiesta: " + dataRichiesta);
      System.out.println("Data Valuta: " + dataValuta);
      System.out.println("Causale: " + causale);
      System.out.println("Tipologia: " + tipologia);
      System.out.println("Importo: " + importo);
    }


    public LocalDate getDataRichiesta() {
        return dataRichiesta;
    }

    public LocalDate getDataValuta() {
        return dataValuta;
    }

    public String getCausale() {
        return causale;
    }

    public String getTipologia() {
        return tipologia;
    }

    public double getImporto() {
        return importo;
    }



}
