
import java.util.Scanner;

public class TerminaleBanca {
  public void effettuaMovimento(ContoCorrente cTaglia, Date dataRichiesta, Date dataValuta, String causale, String tipologia, double importo) {
    Scanner scanner = new Scanner(System.in);

    if(!cTaglia.isBloccato()){
      System.out.println("Vuoi effettuare un depoosito, un prelievo o uscire? (deposito/prelievo/uscita)"");
      String scelta = scanner.nextLine();

        if (scelta.equals("deposito")){
          cTaglia.deposito(importo);
          Movimento movimento = new Movimento(dataRichiesta, dataValuta, causale, tipologia, importo);
          movimento.visulizzaDatiMovimento();
        } 
        else if(scelta.equals("prelievo")){
          cTaglia.prelievo(importo);
          Movimento movimento = new Movimento(dataRichiesta, dataValuta, causale, tipologia, importo);
          movimento.visulizzaDatiMovimento();
        } 
        else if(scelta.equals("prelievo")){
          cTaglia.prelievo(importo);
          Movimento movimento = new Movimento(dataRichiesta, dataValuta, causale, tipologia, importo);
          movimento.visulizzaDatiMovimento();
        } 
    }

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);

        ContoCorrente cTaglia = new ContoCorrente(2500);
        System.out.println("Saldo: " + cTaglia.getSaldo());

        System.out.println("Inserisci \n1.Per prelevare\n2. Per depositare");
        int scelta = scanner.nextInt();

        switch (scelta) {
            case 1:
                if (cTaglia.isContoBloccato()) {
                    System.out.println("Il conto è bloccato. Impossibile effettuare prelievi.");
                } else {
                    System.out.println("Inserisci l'importo da prelevare: ");
                    double importoPrelievo = scanner.nextDouble();
                    cTaglia.prelievo(importoPrelievo);
                }
                break;

            case 2:
                if (cTaglia.isContoBloccato()) {
                    System.out.println("Il conto è bloccato. Impossibile effettuare depositi.");
                } else {
                    System.out.println("Inserisci l'importo da depositare: ");
                    double importoDeposito = scanner.nextDouble();
                    cTaglia.deposito(importoDeposito);
                }
                break;

                 System.out.println("Scelta non valida.");
        }
    }
}
}
