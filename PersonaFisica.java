class PersonaFisica {
    private String nome;
    private String cognome;
    private int eta;

    public PersonaFisica(String nome, String cognome, int eta) {
        this.nome = nome;
        this.cognome = cognome;
        this.eta = eta;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }
    public String toString() {
        return "Nome: " + nome + "\nCognome: " + cognome + "\nEtà: " + eta;
    }
}

public class Main {
    public static void main(String[] args) {
        PersonaFisica persona = new PersonaFisica("Mario", "Rossi", 30);
        System.out.println("Dati della persona:");
        System.out.println(persona.toString());
